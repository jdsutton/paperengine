import('/src/js/level/DemoLevel.js');

/**
 * @class
 */
class GameDriver {

    /**
     * @public
     */
    static init() {
        this.level = new DemoLevel();

        this.level.load().then(() => {
            this.render();
        });
    }

    static getCameraController() {
        return this.level.cameraController.get();
    }

    /**
     * @public
     */
    static render() {
        requestAnimationFrame(() => {
            this.render();
        });

        this.level.render();
    }

    /**
     * @public
     */
    static getCurrentLevel() {
        return this.level;
    }
}

GameDriver.cameraController = null;

window.onload = () => { GameDriver.init(); };

