class Material {
    constructor(texture) {
        this._texture;
    }

    get() {
        return new THREE.MeshBasicMaterial({ map: this._texture });
    }
   
    static fromTexture(tex, transparent) {
        let result =  new THREE.MeshBasicMaterial({ map: tex });

        result.transparent = transparent || false;

        return result;
    } 
}
