class CameraController {
    constructor(viewWidth, viewHeight, fov) {
        this.renderer = new THREE.WebGLRenderer({
            alpha: true
        });
        this.renderer.setSize(viewWidth, viewHeight);
        document.body.appendChild(this.renderer.domElement);
        this.renderer.setClearColor( 0xffffff, 0);

        this.camera = new THREE.PerspectiveCamera(
            fov, viewWidth / viewHeight, 0.1, 1000
        );
        this.camera.rotation.set(0, 0, 0);
    }

    getRenderer() {
        return this.renderer;
    }

    get() {
        return this.camera;
    }

    setPosition(x, y, z) {
        // this._yawObject.position.set(x, y, z);
    }

    getObject() {
        return this._yawObject;
    }
}