import('Material.js');
import('TextureLoader.js');

/**
 * @class
 */
class Animation {

    /**
     * @constructor
     */
    constructor(frameURIs, framerate, transparent, loop) {
        this._index = 0;
        this._framerate = framerate || 24;
        this._frameDurationMs = 1000 / this._framerate;
        this._transparent = transparent || true;
        this._loop = loop;
        if (this._loop === undefined) this._loop = true;

        frameURIs.forEach(uri => {
            TextureLoader.load(uri).then(texture => {
                let material = Material.fromTexture(texture);
                material.transparent = this._transparent;
                this._materials.push(material);
            });
        });
        
        this._materials = [];
    }

    /**
     * @public
     */
    getFrame() {
        const t = new Date().getTime();
        let index = Math.floor(t / this._frameDurationMs);

        if (this._loop) {
            index %= this._materials.length;
        } else {
            index = Math.min(index, this._materials.length - 1);
        }

        return this._materials[index];
    }
}