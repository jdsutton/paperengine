/**
 * Represents a 3D rectangular bounding box.
 * @class
 * @param {Number} x
 * @param {Number} y
 * @param {Number} z
 * @param {Number} w
 * @param {Number} h
 * @param {Number} d
 */
class BoundingBox {
    
    /**
     * @constructor
     */
    constructor(x, y, z, w, h, d) {
        this._x = x;
        this._y = y;
        this._z = z;
        this._w = w;
        this._h = h;
        this._d = d;
    }

    /**
     * @public
     */
    distance(other) {
        const x = Math.pow(this._x - other._x, 2);
        const y = Math.pow(this._y - other._y, 2);
        const z = Math.pow(this._z - other._z, 2);

        return Math.sqrt(x + y + z);
    }

    /**
     * @param {BoundingBox}
     * @return {Boolean}
     */
    isCollidingWith(other) {
        return (this._x <= (other._x + other._w) && (this._x + this._w) >= other._x) &&
            (this._y <= (other._y + other._h) && (this._y + this._h) >= other._y) &&
            (this._z <= (other._z + other._d) && (this._z + this._d) >= other._z);
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @param {Number} z
     */
    setCoords(x, y, z) {
        this._x = x;
        this._y = y;
        this._z = z;
    }

    get y() {
        return this._y;
    }

    get h() {
        return this._h;
    }

    /**
     * @public
     */
    translate(x, y, z) {
        return new BoundingBox(this._x + x, this._y + y, this._z + z,
            this._w, this._h, this._d);
    }
}