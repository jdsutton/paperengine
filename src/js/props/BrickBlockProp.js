import('BlockProp.js');

/**
 * @class
 */
class BrickBlockProp extends BlockProp {
    
    /**
     * @constructor
     */
    constructor(x, y, z) {
        const uri = '/animations/environment/brick_block.png';
        
        super(x, z, uri, uri);
    }
}