import('BlockProp.js');

/**
 * @class
 */
class LargeBlockProp extends BlockProp {

    /**
     * @constructor
     */
    constructor(x, z) {
        super(x, z, LargeBlockProp._IMAGES.FRONT, LargeBlockProp._IMAGES.SIDE,
            64);
    }
}

LargeBlockProp._IMAGES = {
    FRONT: '/animations/environment/block_large.png',
    SIDE: '/animations/environment/block_large_side.png',
};