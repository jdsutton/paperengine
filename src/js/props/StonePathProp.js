[[__top__/src/js/Prop.js]]

class StonePathProp extends Prop {
    constructor(x, z) {
        super(x, z, null, false);

        const pathUri = '/animations/ground/stone_path.png';
        this._props = [];

        let pathSize = 64;

        for (let i = 0; i < 10; i++) {
            const pathTile = new Prop(0, 0, pathUri)
                .setPosition(x + (pathSize / 50) * i, 0.001, z)
                .setRotation(-Math.PI / 2, 0, 0)
                .setSize(pathSize, pathSize)
                .setTransparent(true);

            this._props.push(pathTile); 
        }

        this.setTransparent(true);
    }

    getMeshes() {
        return this._props.map(prop => prop.mesh);
    }

    /**
     * @override
     */
    isCollidingWith(other) {
        return false;
    }
}