import('/src/js/Prop.js');

/**
 * @class
 */
class BlockProp extends Prop {

    /**
     * @constructor
     */
    constructor(x, z, frontImageURI, sideImageURI, size) {
        super(x, z);

        this._props = [];

        size = size || 32;
        this.setSize(size, size);

        this.setImage(frontImageURI);

        const blockSide0 = new Prop(x, z, sideImageURI)
            .setRotation(0, -Math.PI / 2, 0)
            .translate(-this.width / 2, this.height / 2, -this.width / 2);
        this._props.push(blockSide0);

        const blockSide1 = new Prop(x, z, sideImageURI)
            .setRotation(0, Math.PI / 2, 0)
            .translate(this.width / 2, this.height / 2, -this.width / 2);
        this._props.push(blockSide1);

        const blockTop = new Prop(x, z, sideImageURI)
            .setRotation(-Math.PI / 2, 0, 0)
            .translate(0, this.width, -this.width / 2);
        this._props.push(blockTop);
    }
}
