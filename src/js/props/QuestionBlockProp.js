[[__top__/src/js/Prop.js]]

class QuestionBlockProp extends Prop {
    constructor(x, y, z) {
        super(x, z);

        this._props = [];
        const uri = '/animations/environment/question_block.png';

        const front = new Prop(0, 0, uri)
            .setPosition(x, y + (32 / 50) / 2, z + (32 / 50) / 2);
        this._props.push(front);

        const blockSide0 = new Prop(0, 0, uri)
            .setRotation(0, -Math.PI / 2, 0)
            .setPosition(x - (32 / 50) / 2, y + (32 / 50) / 2, z);
        this._props.push(blockSide0);

        const blockSide1 = new Prop(0, 0, uri)
            .setRotation(0, Math.PI / 2, 0)
            .setPosition(x + (32 / 50) / 2, y + (32 / 50) / 2, z);
        this._props.push(blockSide1);

        const blockSide2 = new Prop(0, 0, uri)
            .setRotation(Math.PI / 2, 0, 0)
            .setPosition(x, y, z);
        this._props.push(blockSide2);
    }

    getMeshes() {
        return this._props.map(prop => prop.mesh);
    }
}