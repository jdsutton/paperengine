import('/src/js/MarioPlayer.js');
import('/src/js/npcs/BobombNPC.js');
import('/src/js/npcs/ToadNPC.js');
import('/src/js/props/BrickBlockProp.js');
import('/src/js/props/LargeBlockProp.js');
import('/src/js/props/QuestionBlockProp.js');
import('/src/js/props/StonePathProp.js');
import('Level.js');

/**
 * @public
 */
class DemoLevel extends Level {

    /**
     * @constructor
     */
    constructor() {
        super();

        this.setBackdrop('/backdrops/0.png');
        this.setFloorTexture('/animations/ground/grass.png');
        this.setMusic('/audio/toad_town_theme.mp3');

        this.addProp(new BrickBlockProp(3, 0)
            .translate(0, 2, 0));
        // this.addProp(new QuestionBlockProp(3 + (32 / 50), 0));
        this.addProp(new BrickBlockProp(3 + (32 / 50) * 2, 0)
            .translate(0, 2, 0));
        this.addProp(new BrickBlockProp(3 + (32 / 50), 0)
            .translate(0, 2, 0));

        // Player.
        this.player = new MarioPlayer(2, 2);

        // Props.
        for (let i = 0; i < 32; i++) {
            let x = Math.random() * 32 - 16;
            let z = Math.random() * 32 - 16;

            // TODO: Encapsulate into prop.
            let testProp = new Prop(0, 0, '/animations/bush/bush_0.png', false)
                .setPosition(x, (32 / 50) / 2, z);
            testProp.isTransparent = true;

            this.addProp(testProp);
        }
        this.addProp(this.player);

        let block = new LargeBlockProp(0, 0);
        this.addProp(block);
        block = new LargeBlockProp(-64/50, 0);
        this.addProp(block);
        block = new LargeBlockProp(-64/50, 0)
            .translate(0, 64/50, 0);
        this.addProp(block);

        const path = new StonePathProp(1, 0);
        this.addProp(path);

        // NPCs.
        let toad = new ToadNPC(1, 1);
        this.setDialog(toad, '/text/test_dialogue.txt');
        this.addNPC(toad);

        let toad2 = new ToadNPC(-1, 2);
        this.setDialog(toad2, '/text/big_business_bill.txt');
        this.addNPC(toad2);

        let bobomb = new BobombNPC(1, 2);
        this.setDialog(bobomb, '/text/generic_bobomb.txt');
        this.addNPC(bobomb);
    }
}