import('/src/js/AudioLoader.js');
import('/src/js/dialogue/DialogueFactory.js');
import('/src/js/TextureLoader.js');
import('/src/js/JSUtils/util/Dom.js');
import('/src/js/Prop.js');
import('/src/js/CameraController.js');

/**
 * @class
 */
class Level {

    /**
     * @constructor
     */
    constructor(config) {
        this.scene = new THREE.Scene();
        this._props = [];
        this._transparentMeshes = [];
        this._backdropURI = null;
        this.cameraController = new CameraController(
            this.constructor.VIEWPORT_WIDTH, this.constructor.VIEWPORT_HEIGHT,
            this.constructor.FOV);
        this.renderer = this.cameraController.getRenderer();
        this._promises = [];
        this.player = null;
        this._npcs = [];
    }

    /**
     * @public
     * @param {NPC} npc
     * @param {String} dialogURI
     * @return {Level} - this
     */
    setDialog(npc, dialogURI) {
        const promise = DialogueFactory.produce(dialogURI).then(dialogue => {
            npc.setDialogue(dialogue);
        });

        this.waitFor(promise);
    }

    /**
     * @public
     * @param {Promise}
     * @return {Level} - this
     */
    waitFor(promise) {
        this._promises.push(promise);

        return this;
    }

    /**
     * @public
     * @return {Promise}
     */
    load() {
        const audio = AudioLoader.loadAll();
        const textures = TextureLoader.loadAll();

        this._promises.push(audio);
        this._promises.push(textures);

        return Promise.all(this._promises).then(() => {
            this._promises = [];
        });
    }

    /**
     * @public
     * @param {NPC} npc
     * @return {Level} - this
     */
    addNPC(npc) {
        this._npcs.push(npc);

        this.addProp(npc);
    }

    /**
     * @public
     */
    render() {
        if (this.player) this.player.update();

        this._npcs.forEach(npc => {
            npc.update();
        });

        if (this._transparentMeshes) {
            // TODO: Sort by depth.
            this._transparentMeshes.forEach(mesh => {
                this.scene.add(mesh);
            });
            
            this._transparentMeshes = [];
        }       

        this.renderer.render(this.scene, GameDriver.getCameraController());
    }

    /**
     * @public
     * Add a Prop to the level.
     * @param {Prop} prop
     */
    addProp(prop) {
        prop.getMeshes().forEach(mesh => {
            if (prop.isTransparent) {
                // Defer until end.
                this._transparentMeshes.push(mesh);
            } else {
                this.scene.add(mesh)
            }
        });

        this._props.push(prop);
    }

    /**
     * @public
     * @return {Prop[]}
     */
    getProps() {
        return this._props;
    }

    /**
     * @public
     */
    setBackdrop(uri) {
        const promise = TextureLoader.load(uri).then(texture => {
            let img = texture.image;

            img.width = this.constructor.VIEWPORT_WIDTH;
            img.height = this.constructor.VIEWPORT_HEIGHT;

            Dom.setContents('backdropContainer', img);
        });

        this._promises.push(promise);

        return this;        
    }

    /**
     * @public
     */
    setFloorTexture(uri, width, height) {
        width = width || 640;
        height = height || 480;

        const groundGeometry = new THREE.PlaneBufferGeometry(width, height, 32);

        const promise = TextureLoader.load(uri).then(texture => {
            texture.wrapS = THREE.RepeatWrapping;
            texture.wrapT = THREE.RepeatWrapping;
            texture.repeat.set(50, 50);

            const groundMaterial = Material.fromTexture(texture);
            const ground = new THREE.Mesh(groundGeometry, groundMaterial);
            ground.position.y = 0;
            ground.rotation.x = -Math.PI * 0.5;

            this.scene.add(ground);
        });

        this._promises.push(promise);

        return this;
    }

    /**
     * @public
     */
    setMusic(uri) {
        const promise = AudioLoader.load(uri).then(audio => {
            audio.play();
        });

        this._promises.push(promise);

        return this;
    }
};

Level.VIEWPORT_WIDTH = 640;
Level.VIEWPORT_HEIGHT = 480;
Level.FRAME_RATE = 60;
Level.FOV = 75;