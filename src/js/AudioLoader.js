/**
 * @class
 */
class AudioLoader {

    /**
     * @public
     */
    static loadAll() {
        return Promise.all(this._promises);
    }

    /**
     * @public
     */
    static load(uri) {
        if (this.audioByUri[uri]) {
            return Promise.resolve(this.audioByUri[uri])
        }

        const promise = new Promise((resolve, reject) => {
            let audio = new Audio(uri);
            this.audioByUri[uri] = audio;

            audio.addEventListener('ended', () => {
                if (this._loopByUri[uri]) {
                    audio.currentTime = 0;
                    audio.play();
                }
            }, false);

            audio.addEventListener('canplaythrough', () => {
                resolve(audio);
            });
        });

        this._promises.push(promise);

        return promise;
    }

    /**
     * @public
     */
    static get(uri) {
        return this.audioByUri[uri];
    }

    /**
     * @public
     * @param {String} uri
     * @param {Boolean} loop - default false
     */
    static playSound(uri, loop) {
        this.audioByUri[uri].play();

        this._loopByUri[uri] = Boolean(loop);
    }

    /**
     * @public
     * @param {String} uri
     */
    static stopSound(uri) {
        this.audioByUri[uri].pause();
        this.audioByUri.currentTime = 0;
        this._loopByUri[uri] = false;
    }
}

AudioLoader._promises = [];
AudioLoader.audioByUri = {};
AudioLoader._loopByUri = {};