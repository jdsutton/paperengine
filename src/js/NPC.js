import('Prop.js');

/**
 * @class
 */
class NPC extends Prop {

    /**
     * @constructor
     */
    constructor(x, z) {
        super(x, z, null, false);

        this.gravity = -0.01;
        this.vy = 0;
        this.vx = 0;
        this.vxPrevious = 0;
        this.vz = 0;
        this.vzPrevious = 0;
        this.facing = NPC.DIRECTION.LEFT;
        this.speed = 0.015;
        this._dialogue = null;
        this._animation = null;
        this._canMove = true;
        this.spin = 0;
        this._spinStartTime = null;
        this._initialRotation = null;
        this._depth = 0.1;

        this.setDoubleSided();

        setTimeout(() => {
            this._startMoving();
        }, Math.random() * 1000 + 1000);
    }

    /**
     * @private
     */
    _handleSpin() {
        // If direction changed, start spinning.
        const startedFacingLeft = (this.vx < 0 && this.facing === NPC.DIRECTION.RIGHT);
        const startedFacingRight = (this.vx > 0 && this.facing === NPC.DIRECTION.LEFT);
        const spinTimeMS = 300;

        const waitStopSpin = () => {
            this._spinStartTime = new Date().getTime();
            this._initialRotation = this.mesh.rotation.y;
            
            setTimeout(() => {

                this.spin = 0;

                if (this.facing === NPC.DIRECTION.LEFT) {
                    this.mesh.rotation.y = 0;
                } else {
                    this.mesh.rotation.y = Math.PI;
                }
            }, spinTimeMS);
        };

        if (startedFacingRight) {
            this.facing = NPC.DIRECTION.RIGHT;
            this.spin = 1;

            waitStopSpin();
        } else if (startedFacingLeft) {
            this.facing = NPC.DIRECTION.LEFT;
            this.spin = -1;
            
            waitStopSpin();
        }

        if (this.spin) {
            const t = new Date().getTime();
            const spinIncrement = (t - this._spinStartTime) / spinTimeMS;
            const rotation = Math.PI * spinIncrement * this.spin;

            this.setRotation(null, this._initialRotation + rotation, null);
        }
    }

    /**
     * @private
     */
    _onMove() {

    }

    /**
     * @abstract
     */
    _onStop() {}

    /**
     * @abstract
     */
    _onJump() {}

    /**
    * @abstract
    */
    _onLand() {}

    /**
     * @private
     */
    _startMoving() {
        if (!this._canMove) {
            setTimeout(() => {
                this._startMoving();
            }, Math.random() * 5000 + 1000);

            return;
        }
        const direction = Math.random() * 2 * Math.PI;
        
        this.vx = this.speed * Math.cos(direction);
        this.vz = this.speed * -Math.sin(direction);

        this._onMove();

        setTimeout(() => {
            this._stopMoving();
        }, Math.random() * 5000 + 1000);
    }

    /**
     * @private
     */
    _stopMoving() {
        this.vx = 0;
        this.vz = 0;

        this._onStop();

        setTimeout(() => {
            this._startMoving();
        }, Math.random() * 5000 + 1000);
    }

    /**
     * @public
     * @param {Dialogue} dialogue
     * @return {NPC} - this
     */
    setDialogue(dialogue) {
        this._dialogue = dialogue;

        return this;
    }

    /**
     * @public
     */
    interact() {
        if (this._dialogue && !Dialogue.isOpen()) {
            this._stopMoving();
            this._canMove = false;
            this._dialogue.show();

            this._dialogue.onClosed(() => {
                this._canMove = true;
            });
        }
    }

    /**
     * @public
     */
    jumping() {
        return this.vy !== 0;
    }

    /**
     * @public
     */
    wasMoving() {
        return this.vzPrevious || this.vxPrevious;
    }

    /**
     * @public
     */
    moving() {
        const eta = 0.0001;

        return Math.abs(this.vz) > eta || Math.abs(this.vx) > eta; 
    }

    /**
     * @public
     */
    update() {
        if (this._animation) {
            const frame = this._animation.getFrame();
            
            this.setMaterial(frame);
        }

        this._handleSpin();

        // Move.
        this.translate(this.vx, this.vy, this.vz);
    }

    setAnimation(animation) {
        this._animation = animation;

        const frame = this._animation.getFrame();
        const image = frame.map.image;
        this.setSize(image.width * this.scale, image.height * this.scale);

        if (this.mesh.position.y === 0) {
            this.setPosition(null, this.height / 2, null);
        }

        return this;
    }

    /**
     * @public
     */
    jump() {
        if (this.vy === 0) {
            this.vy = 0.2;
            this._onJump();
        }
    };
}

NPC.DIRECTION = {
    LEFT: 0,
    RIGHT: 1
};