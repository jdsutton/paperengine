import('/src/js/JSUtils/util/Keys.js');
import('BoundingBox.js');
import('NPC.js');

class Player extends NPC {
    constructor(x, z) {
        super(x, z);

        this.speed = 0.06;

        this.setTransparent(true);

        Key.onKeyReleased(KEYS.E_KEY, () => {
            this._tryInteract();
        });

        Key.onKeyPressed(KEYS.SPACE, () => {
            this._handleSpacebarPressed();
        });

        Key.onKeyReleased(KEYS.SPACE, () => {
            this._handleSpacebarReleased();
        });
    }

    /**
     * @override
     */
    _startMoving() {}

    /**
     * @private
     */
    _handleSpacebarPressed() {
        this.jump();
    }

    /**
     * @private
     */
    _handleSpacebarReleased() {
        if (this.vy > 0) this.vy = 0;
    }

   /**
    * @private
    */
    _tryInteract() {
        const props = GameDriver.getCurrentLevel().getProps();

        // Find closest prop.
        let closest = null;
        let closestDistance = this.constructor._INTERACT_DISTANCE + 1;

        props.forEach(prop => {
            if (prop === this) return;
            if (!prop.interactable) return;

            const distance = this.boundingBox.distance(prop.boundingBox);

            if (distance < closestDistance) {
                closestDistance = distance;
                closest = prop;
            }
        });

        if (closest && closestDistance < this.constructor._INTERACT_DISTANCE) {
            closest.interact();
        }
    }

   /**
    * @private
    */
    _lowerOntoObject(prop) {
        const top = prop.boundingBox.y + prop.boundingBox.h;
        this.mesh.position.y = top + 0.001;

        this._setBoundingBoxPosition();
    }

   _handleLanding(collidedWith) {
        if (collidedWith) {
            // Land on object.
            this._lowerOntoObject(collidedWith);
        } else {
            // Land on ground.
            this.mesh.position.y = 0.5;
            this._setBoundingBoxPosition();
        }

        this.vy = 0;
        this._onLand();
   }

    _positionCollidesWithObject(boundingBox) {
        const props = GameDriver.getCurrentLevel().getProps();

        for (let prop of props) {
            if (prop === this) continue;
            if (!prop.isCollideable) continue;

            if (prop.isCollidingWith(boundingBox)) {
                return prop;
            }
        }

        return false;
    }

    /**
     * @private
     */
    _handleHorizontalMovement() {
        // Movement.  
        this.vx = 0;
        this.vz = 0;

        if (Key.isDown(KEYS.A_KEY)) this.vx -= this.speed;
        if (Key.isDown(KEYS.D_KEY)) this.vx += this.speed;
        if (Key.isDown(KEYS.W_KEY)) this.vz -= this.speed;
        if (Key.isDown(KEYS.S_KEY)) this.vz += this.speed;

        const magnitude = Math.sqrt(Math.pow(this.vx, 2) + Math.pow(this.vz, 2));

        if (magnitude) {
            this.vx = this.vx / magnitude * this.speed;
            this.vz = this.vz / magnitude * this.speed;
        }

        const directionChanged = (Math.sign(this.vx) !== Math.sign(this.vxPrevious)
            || Math.sign(this.vz) !== Math.sign(this.vzPrevious));
        if (directionChanged && this.moving()) this._onMove();
        else if (!this.moving() && this.wasMoving()) this._onStop();

        // Handle horizontal collisions.
        const nextPosition = this.boundingBox.translate(this.vx, 0, this.vz);
        const willCollide = this._positionCollidesWithObject(nextPosition);
        
        this.vxPrevious = this.vx;
        this.vzPrevious = this.vz;

        if (!willCollide) {
            this.translate(this.vx, 0, this.vz);
        }
    }

    /**
     * @private
     */
    _handleVerticalMovement() {
        // Handle vertical collisions.
        const falling = this.vy < 0;

        if (falling) {
            const nextPosition = this.boundingBox.translate(0, this.vy, 0);
            const willCollideWith = this._positionCollidesWithObject(nextPosition);
            const hitGround = this.mesh.position.y < 0.5;

            if (hitGround || willCollideWith) {
                this._handleLanding(willCollideWith);
            }
        } else if (this.vy > 0) {
            // Bonk head on things.
            const nextPosition = this.boundingBox.translate(0, this.vy, 0);
            const willCollideWith = this._positionCollidesWithObject(nextPosition);

            if (willCollideWith) {
                this.vy = 0;
            }

        } else if (this.mesh.position.y > 0.5) {
            // Handle falling off ledges.
            const nextPosition = this.boundingBox.translate(0, this.gravity, 0);
            const willCollideWith = this._positionCollidesWithObject(nextPosition);
        
            if (!willCollideWith) {
                this.vy += this.gravity;
            }
        }

        // Apply gravity and fall.
        if (this.jumping()) this.vy += this.gravity;

        this.translate(0, this.vy, 0);
    }

    update() {
        // TODO: Handle control input here.

        // TODO: Move this to NPC.
        this._handleHorizontalMovement();
        this._handleVerticalMovement();
        this._handleSpin();
        this.setMaterial(this._animation.getFrame());

        this.aimCamera();
    };

    aimCamera() {
        let camera = GameDriver.getCameraController();

        camera.position.z = this.mesh.position.z + 5;
        camera.position.x = this.mesh.position.x;
        camera.position.y = this.mesh.position.y + 2;
    };

    isCollidingWith(other) {
        return other.isCollidingWith(this.boundingBox);
    }
}

Player._INTERACT_DISTANCE = 1.0;
