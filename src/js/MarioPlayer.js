[[Animation.js]]
[[AudioLoader.js]]
[[Player.js]]

class MarioPlayer extends Player {
    constructor(x, z) {
        super(x, z);
       
        this.setAnimation(MarioPlayer.ANIMATION.IDLE_LEFT);

        this.constructor._JUMP_SOUNDS.forEach(uri => {
            AudioLoader.load(uri);
        });
    }

    _onJump() {
        this._setAnimationJump();

        const uris = this.constructor._JUMP_SOUNDS;
        const index = Math.floor(Math.random() * uris.length);
        const uri = uris[index];

        AudioLoader.playSound(uri);
    }

    _setAnimationJump() {
        this.setAnimation(MarioPlayer.ANIMATION.JUMP_LEFT);
    }

    _setAnimationIdle() {
        this.setAnimation(MarioPlayer.ANIMATION.IDLE_LEFT);
    }

    _setAnimationWalk() {
        this.setAnimation(MarioPlayer.ANIMATION.WALK_LEFT);
    }

    _onMove() {
        if (!this.jumping()) this._setAnimationWalk();
    }

    _onStop() {
        if (!this.jumping()) this._setAnimationIdle();
    }

    _onLand() {
        if (this.moving()) {
            this._setAnimationWalk()
        } else {
            this._setAnimationIdle();
        }
    }

    update() {
        super.update();

        // TODO: If can interact, show alert.
    }
}

MarioPlayer.ANIMATION = {
    WALK_LEFT: new Animation([
        '/animations/mario_walk_left/0.png',
        '/animations/mario_walk_left/1.png',
        '/animations/mario_walk_left/2.png',
        '/animations/mario_walk_left/3.png',
    ]),
    IDLE_LEFT: new Animation([
        '/animations/mario_idle_left/0.png',
        '/animations/mario_idle_left/1.png',
    ], 2 /* FPS */),
    JUMP_LEFT: new Animation([
        '/animations/mario_jump_left/0.png',
        '/animations/mario_jump_left/1.png',
        '/animations/mario_jump_left/2.png',
        '/animations/mario_jump_left/3.png',
    ], 24 /* FPS */, true /* Transparent */, false /* Loop */),
};

MarioPlayer._JUMP_SOUNDS = [
    '/audio/jump_10.mp3',
    '/audio/jump_11.mp3',
    '/audio/jump_15.mp3',
];