import('/src/js/Animation.js');
import('/src/js/NPC.js');
import('/src/js/Prop.js');

/**
 * @class
 */
class BobombNPC extends NPC {

    /**
     * @constructor
     */
    constructor(x, z) {
        super(x, z);

        const scale = 0.25;
        const eta = 0.0001;
        this._legLength = 0.6;
        this._footSwingScale = 0.5;

        this.setScale(scale)
            .translate(0, this._legLength, 0)
            .setAnimation(BobombNPC.ANIMATION.IDLE_LEFT);

        const eyes = new Prop(x, z + eta, BobombNPC._IMAGES.EYES)
            .translate(0, this._legLength - 0.1, 0)
            .setScale(scale);
        this._addProp(eyes);

        // TODO: winder.

        this.foot1 = new Prop(x - 0.1, z + eta, BobombNPC._IMAGES.FOOT)
            .setScale(scale);
        this._addProp(this.foot1);

        this.foot2 = new Prop(x + 0.1, z + eta, BobombNPC._IMAGES.FOOT)
            .setScale(scale);
        this._addProp(this.foot2);

        this.setInteractable(true);
    }

    /**
     * @private
     */
    _moveFeet() {
        const r = this._legLength;
        const t = Math.sin((new Date().getTime()) / 100) * this._footSwingScale
            - Math.PI / 2;
        const x = Math.cos(t) * r;
        const y = Math.sin(t) * r + this._legLength;

        this.foot1.resetPosition();
        this.foot1.translateTemporary(x, y, 0);
        this.foot1.setRotation(0, 0, t + Math.PI / 2);
        this.foot2.resetPosition();
        this.foot2.translateTemporary(-x, y, 0);
        this.foot2.setRotation(0, 0, -t - Math.PI / 2);
    }
    

    /**
     * @public
     */
    update() {
        super.update();

        if (this.moving()) {
            this._moveFeet();
        }
    }
 
    /**
     * @override
     */
    _onMove() {

    }

    /**
     * @override
     */
    _onStop() {
        this.foot1.resetPosition();
        this.foot1.setRotation(0, 0, 0);
        this.foot2.resetPosition();
        this.foot2.setRotation(0, 0, 0);
    }
}

BobombNPC.ANIMATION = {
    IDLE_LEFT: new Animation([
        '/animations/bobomb/bobomb_body.png',
    ], 1 /* FPS */),
}

BobombNPC._IMAGES = {
    EYES: '/animations/bobomb/eyes.png',
    FOOT: '/animations/bobomb/foot.png',
    WINDER: '/animations/bobomb/winder.png',
};