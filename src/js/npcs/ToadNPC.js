[[__top__/src/js/Animation.js]]
[[__top__/src/js/NPC.js]]

/**
 * @class
 */
class ToadNPC extends NPC {
    constructor(x, z) {
        super(x, z);

        this.setAnimation(ToadNPC.ANIMATION.IDLE_LEFT);

        this.setInteractable(true);
    }
 
    /**
     * @override
     */
    _onMove() {
        this.setAnimation(this.constructor.ANIMATION.WALK_LEFT);
    }

    /**
     * @override
     */
    _onStop() {
        this.setAnimation(this.constructor.ANIMATION.IDLE_LEFT);
    }

}

ToadNPC.ANIMATION = {
    IDLE_LEFT: new Animation([
        '/animations/toad/idle_left/0.png',
        '/animations/toad/idle_left/1.png',
        '/animations/toad/idle_left/2.png',
        '/animations/toad/idle_left/3.png',
    ], 6 /* FPS */),
    WALK_LEFT: new Animation([
        '/animations/toad/walk_left/0.png',
        '/animations/toad/walk_left/1.png',
        '/animations/toad/walk_left/2.png',
        '/animations/toad/walk_left/3.png',
    ], 6 /* FPS */),
}
