[[Material.js]]
[[TextureLoader.js]]

class Prop {
    constructor (x, z, uri, isCollideable) {
        if (isCollideable === undefined) isCollideable = true;

        this.geometry = new THREE.PlaneBufferGeometry(1, 1);
        this.material = new THREE.MeshBasicMaterial({color: 0xff0000});
        this.height = null;
        this.width = null;
        this.mesh = new THREE.Mesh(this.geometry, this.material);
        this.mesh.position.y = 0;
        this.mesh.position.x = x || 0;
        this.mesh.position.z = z || 0;
        this.boundingBox = new BoundingBox(this.mesh.position.x, this.mesh.position.y,
            this.mesh.position.z, this.width, this.height, this.width);
        this._onLoad = null;
        this.isCollideable = isCollideable;
        this._props = [];
        this.isTransparent = false;
        this.interactable = false;
        this.scale = 1.0;
        this._prevMeshPosition = null;
        this._isDoubleSided = false
        this._depth = null;

        if (uri) {
            TextureLoader.load(uri).then(texture => {
                this.setTexture(texture);
                if (this._onLoad) {
                    this._onLoad();
                }
            });
        }
    }

    /**
     * @public
     */
    setDoubleSided() {
        this.mesh.material.side = THREE.DoubleSide;
        this._isDoubleSided = true;

        return this;
    }

    /**
     * @private
     */
    _addProp(prop) {
        this._props.push(prop);
    }

    /**
     * @abstract
     */
    interact() {

    }

    /**
     * @public
     * @param {Boolean} value
     * @return {Prop} - this
     */
    setTransparent(value) {
        this.isTransparent = value;

        return this;
    }

    /**
     * @public
     * @param {Boolean} value
     * @return {Prop} - this
     */
    setInteractable(value) {
        this.interactable = value;

        return this;
    }

    /**
     * @public
     */
    setTexture(texture) {
        if (!(this.width && this.height)) {
            this.width = texture.image.naturalWidth / 50 * this.scale;
            this.height = texture.image.naturalHeight / 50 * this.scale;
        }

        this.material = Material.fromTexture(texture, true);
        this.setMaterial(this.material);

        this._resize();

        return this;
    }

    /**
     * @public
     */
    setMaterial(material) {
        this.mesh.material = material;

        if (this._isDoubleSided) this.setDoubleSided();
    }

    /**
     * @public
     * @return {Promise}
     */
    setImage(uri) {
        return TextureLoader.load(uri).then(texture => {
            this.setTexture(texture);
        });
    }

    /**
     * @public
     */
    getMeshes() {
        const mesh = [this.mesh];
        const propMeshes = this._props.map(prop => {
            return prop.mesh;
        });
    
        return mesh.concat(propMeshes);
    };

    /**
     * @public
     */
    setRotation(x, y, z) {
        if (x !== null) this.mesh.rotation.x = x;
        if (y !== null) this.mesh.rotation.y = y;
        if (z !== null) this.mesh.rotation.z = z;

        // TODO: Recurse.


        return this;
    }

    /**
     * @public
     */
    rotate(x, y, z) {
        this.mesh.rotation.x += x;
        this.mesh.rotation.y += y;
        this.mesh.rotation.z += z;

        // TODO: Recurse.


        return this;
    }

    /**
     * @public
     */
    setPosition(x, y, z) {
        x = x || this.mesh.position.x;
        y = y || this.mesh.position.y;
        z = z || this.mesh.position.z;

        const dx = x - this.mesh.position.x;
        const dy = y - this.mesh.position.y;
        const dz = z - this.mesh.position.z;

        this.mesh.position.x = x;
        this.mesh.position.y = y;
        this.mesh.position.z = z;

        this._setBoundingBoxPosition();

        this._props.forEach(prop => {
            prop.translate(dx, dy, dz);
        });

        return this;
    }

    _resize() {
        this.mesh.geometry = new THREE.PlaneBufferGeometry(this.width, this.height);
        
        const depth = this._depth || this.width;
        this.boundingBox = new BoundingBox(
            this.mesh.position.x,
            this.mesh.position.y,
            this.mesh.position.z - depth,
            this.width, this.height, depth);

        if (this.mesh.position.y === 0 && this.height) {
            this.setPosition(null, this.height / 2, null);
        }
    }

    /**
     * @public
     */
    setSize(w, h) {
        this.width = w / 50;
        this.height = h / 50;

        this._resize();

        return this;
    }

    setScale(s) {
        this.width *= s;
        this.height *= s;

        this.scale = s;

        this._resize();

        return this;
    }

    /**
     * @public
     */
    setOnLoad(f) {
        this._onLoad = f;
    }

    /**
     * @public
     */
    getImage() {
        return this.mesh.material.map.image;
    }

    /**
     * @public
     */
    isCollidingWith(other) {
        if (this.isCollideable && this.boundingBox.isCollidingWith(other)) {
            return true;
        }
    }

    /**
     * @public
     */
    isCollidingWith(other) {
        return this.boundingBox.isCollidingWith(other);
    }

    /**
     * @private
     */
    _setBoundingBoxPosition() {
        this.boundingBox.setCoords(this.mesh.position.x, this.mesh.position.y,
            this.mesh.position.z);
    }

    /**
     * @public
     */
    translate(x, y, z) {
        this.mesh.position.x += x;
        this.mesh.position.y += y;
        this.mesh.position.z += z;
        this._setBoundingBoxPosition();

        if (this._prevMeshPosition) {
            this._prevMeshPosition.x += x;
            this._prevMeshPosition.y += y;
            this._prevMeshPosition.z += z;
        }

        this._props.forEach(prop => {
            prop.translate(x, y, z);
        });

        return this;
    }

    /**
     * @public
     */
    translateTemporary(x, y, z) {
        this._prevMeshPosition = {
            x: this.mesh.position.x,
            y: this.mesh.position.y,
            z: this.mesh.position.z
        };

        this.mesh.position.x += x;
        this.mesh.position.y += y;
        this.mesh.position.z += z;

        // TODO: Recurse.
    }

    /**
     * @public
     */
    resetPosition() {
        if (this._prevMeshPosition) {
            Object.assign(this.mesh.position, this._prevMeshPosition);

            this._prevMeshPosition = null;
        }
    }
}