import('/src/js/JSUtils/util/Dom.js');
import('/src/js/JSUtils/util/Keys.js');
import('/src/js/AudioLoader.js');

/**
 * @class
 */
class Dialogue {

    /**
     * @constructor
     */
    constructor(text, options, children) {
        this._text = text;
        this._options = options;
        this._children = children;
        this._index = 0;
        this._interval = null;
        this._onClosed = [];
    }

    /**
     * @public
     * @param {Function} callback
     */
    onClosed(callback) {
        this._onClosed.push(callback);
    }

    /**
     * @public
     * @return {Dialogue}
     */
    toString() {
        let result = '';

        if (this._text) {
            result = result + this._text + '\n';
        }

        if (this._options) {
            for (let i = 0; i < this._options.length; i++) {
                const option = this._options[i];

                result = result + String(i) + '. ' + option + '\n';
            }
        }

        return result;
    }

    /**
     * @public
     * @return {Boolean}
     */
    isTerminal() {
        return !this._options;
    }

    /**
     * @public
     * @param {Number} i
     * @return {Dialogue}
     */
    getChild(i) {
        return this._children[i];
    }

    /**
     * @public
     * @return {Promise}
     */
    show() {
        if (!Dialogue.getLock(this)) {
            return;
        }

        // TODO: Show image.

        // Spell out text.
        this._index = 0;

        this._interval = setInterval(() => {
            this._typeText();
        }, this.constructor._characterTypeDelayMs);

        AudioLoader.playSound(this.constructor._TEXT_READOUT_FX_URI, /** loop */ true);
    }

    /**
     * @private
     */
    _getFullText() {
        let text = this._text || '...';
        let i = 0;
        let c = 'Option';

        this._options.forEach(option => {
            let c2 = c;

            if (i === Dialogue._selectedOptionIndex) {
                c2 = c2 + ' Selected';
            }

            text = `${text}\n\t<span class="${c2}">${option}</span>`;

            i++;
        });

        return text;
    }

    /**
     * @public
     */
    finishedTyping() {
        const text = this._getFullText();
        
        return this._index >= text.length - 1;  
    }

    /**
     * @public
     */
    finishTyping() {
        const text = this._getFullText();

        this._index = text.length;

        this._typeText();
    }

    /**
     * @private
     */
    _handleClose() {
        this.stopTyping();

        this._onClosed.forEach(callback => {
            callback();
        });
    }

    /**
     * @private
     */
    _typeText() {
        this._index++;

        let container = Dom.getById(this.constructor._TEXT_CONTAINER);
        const text = this._getFullText();
        const textPart = text.slice(0, this._index);

        Dom.setContents(container, textPart);
        container.style.display = 'initial';

        // Skip over HTML.
        if (text[this._index] === '<') {
            while (text[this._index] !== '>' && this._index < text.length - 1) {
                this._index++;
            }

            this._index++;
        }

        if (this._index >= text.length) {
            this.stopTyping();
        }
    }

    /**
     * @public
     */
    stopTyping() {
        clearInterval(this._interval);

        this._interval = null;
        AudioLoader.stopSound(this.constructor._TEXT_READOUT_FX_URI);
    }

    /**
     * @public
     */
    static getLock(d) {
        if (this._activeDialogue) return false;

        this._activeDialogue = d;
        this._selectedOptionIndex = 0;

        return true;
    }

    static _releaseLock() {
        this._activeDialogue = null;
    }

    /**
     * @private
     */
    static _handleEscapeKey() {
        this.close();
    }

    /**
     * @private
     */
    static _handleNumberKeys() {
        // TODO.
    }

    /**
     * @private
     */
    static _handleDownArrow() {
        if (!this.isOpen()) return;

        const dialogue = this._activeDialogue;

        this._selectedOptionIndex++;
        this._selectedOptionIndex %= dialogue._options.length;

        dialogue._typeText();
    }

    /**
     * @private
     */
    static _handleUpArrow() {
        if (!this.isOpen()) return;
        
        const dialogue = this._activeDialogue;

        this._selectedOptionIndex--;
        this._selectedOptionIndex += dialogue._options.length;
        this._selectedOptionIndex %= dialogue._options.length;

        dialogue._typeText();
    }

    /**
     * @private
     */
    static _handleEnterKey() {
        if (!this.isOpen()) return;

        const dialogue = this._activeDialogue;
        const option = dialogue._options[this._selectedOptionIndex];
        const child = dialogue._children[this._selectedOptionIndex];

        // If still typing, finish typing.
        if (!dialogue.finishedTyping()) {
            dialogue.finishTyping();

            return;
        }

        if (option) {
            // Select option.
            if (!child) {
                // No response.
                console.debug('No response, closing');
                this.close();
            } else {
                this._releaseLock();
                child.show();
                console.debug('Showing:');
                console.debug(this._activeDialogue);
            }
        } else {
            console.debug('No options, closing');
            this.close();
        }
    }

    /**
     * @public
     * @return {Boolean}
     */
    static isOpen() {
        return Boolean(this._activeDialogue);
    }

    /**
     * @public
     * @return {Promise}
     */
    static load() {
        return TextureLoader.load(this._dialogueBubbleImageURI);
    }

    /**
     * @public
     */
    static close() {
        let container = Dom.getById(this._TEXT_CONTAINER);

        if (this._activeDialogue) {
            this._activeDialogue._handleClose();
            this._releaseLock();
        }

        container.style.display = 'none';
        Dom.setContents(container, '');
    }
}

Dialogue._activeDialogue = null;
Dialogue._characterTypeDelayMs = 40;
Dialogue._dialogueBubbleImageURI = '/animations/speech_bubble_large.png';
Dialogue._selectedOptionIndex = 0;
Dialogue._TEXT_READOUT_FX_URI = '/audio/text_readout.mp3';
Dialogue._TEXT_CONTAINER = 'dialogueTextContainer';

AudioLoader.load(Dialogue._TEXT_READOUT_FX_URI);

// Key bindings.
Key.onKeyReleased(KEYS.ESCAPE_KEY, () => {
    Dialogue._handleEscapeKey();
});

Key.onKeyReleased(KEYS.ENTER_KEY, () => {
    Dialogue._handleEnterKey();
});

Key.onKeyReleased(KEYS.ARROW_UP, () => {
    Dialogue._handleUpArrow();
});

Key.onKeyReleased(KEYS.ARROW_DOWN, () => {
    Dialogue._handleDownArrow();
});