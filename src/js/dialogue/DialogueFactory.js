import('/src/js/JSUtils/util/Request.js');
import('Dialogue.js');

/**
 * @class
 */
class DialogueFactory {

    /**
     * @param {String} sourceFile
     * @return {Promise<Dialogue>}
     */
    static produce(sourceFile) {
        return Request.get(sourceFile, /** crossOrigin */ true).then(data => {
            return this._produceFromString(data);
        });
    }

    /**
     * @private
     */
    static _produceFromString(s) {
        let start = s.indexOf('[dialogue]');

        if (start > -1) {
            start = s.indexOf('\n', start) + 1;
            s = s.slice(start, s.length);
        }
        
        // TODO: Trim off part after dialogue.

        const whitespacePattern = /^\s*/;
        const numPattern = /^\s*[0-9]+\. /;
        let lines = s.split('\n');
        let indentation;

        for (let i=0; i < lines.length; i++) {
            const line = lines[i];
            indentation = line.match(whitespacePattern);

            if (indentation && indentation[0].length) break;
        }

        // Combine all lines matching indentation part into Dialogue()
        let text = null;
        let options = [];
        let children = [];

        console.debug(lines);

        for (let i=0; i < lines.length; i++) {
            const line = lines[i];
            const indent = line.match(whitespacePattern);

            console.debug('Consider line: ' + line);

            if (indent[0] === '') {
                // Is text.
                if (!text) text = line;
            } else if (indent[0] === indentation[0] && line.match(numPattern)) {
                // Is option.
                options.push(line.trim());
            
                // Add corresponding child.
                let j;
                let childText = '';

                if (!lines[i+1] || lines[i+1].match(whitespacePattern)[0].length <= indent[0].length) {
                    // No response.
                    console.debug('no response');
                    continue;
                }

                const nextIndent = lines[i+1].match(whitespacePattern);

                if (nextIndent[0].length > indent[0].length) {
                    for (j=i+1; j < lines.length; j++) {
                        const childLine = lines[j];
                        const childIndent = childLine.match(whitespacePattern);

                        console.debug('Child line: ' + childLine);

                        if (childIndent[0].length >= nextIndent[0].length) {
                            childText = childText + childLine.replace(nextIndent[0], '') + '\n';
                        } else {
                            break;
                        }
                    }
                    childText = childText.slice(0, childText.length-1);

                    i = j-1;

                    console.debug('Recurse into:');
                    console.debug(childText);

                    const child = this._produceFromString(childText);
                    children.push(child);
                }
            } else {
                console.warn('Unrecognized dialogue line: ' + line);
            }
        }

        const result = new Dialogue(text, options, children);
        console.debug(result);

        return result;
    }
}