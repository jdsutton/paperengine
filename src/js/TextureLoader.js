class TextureLoader {
    static loadAll() {
        return this._loadMissingTexture().then(() => {
            return Promise.all(this._promises);
        });
    }

    static _loadMissingTexture() {
        return new Promise((resolve, reject) => {
            this._loader.load(this._missingTextureUri, texture => {
                texture.minFilter = THREE.LinearFilter;
                texture.image.width = 48;
                texture.image.height = 48;
                this._texturesByUri[this._missingTextureUri] = texture;

                resolve(texture);
            });
        });
    }

    static load(uri) {
        if (this._texturesByUri[uri]) {
            return Promise.resolve(this._texturesByUri[uri])
        }

        const promise = new Promise((resolve, reject) => {
            this._loader.load(uri, texture => {
                texture.minFilter = THREE.LinearFilter;
                this._texturesByUri[uri] = texture;

                resolve(texture);
            }, (xhr) => { }, err => {
                this._loadMissingTexture().then(texture => {
                    this._texturesByUri[uri] = texture;
                    resolve(texture);
                })
            });
        });

        this._promises.push(promise);

        return promise;
    }

    static get(uri) {
        return this._texturesByUri[uri];
    }
}

TextureLoader._loader = new THREE.TextureLoader();
TextureLoader._promises = [];
TextureLoader._texturesByUri = {};
TextureLoader._missingTextureUri = '/animations/missing_texture.svg';