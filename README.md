# Paper Engine
A game engine for building Paper Mario-style games.

![screenshot](https://pbs.twimg.com/media/DRSZ6rhVoAIKqiv.jpg:large)

## Setup
```
$ git clone --recursive https://github.com/jdsutton/paperengine.git
$ cd paperengine
# Install Python 3.* if you don't have it.
```

## Textures and Audio
* [Download here](https://drive.google.com/drive/folders/1wsh0BIK3XJosrqv7NkZXk202qZQYVmrT?usp=sharing)
* Place the downloaded folders under paperengine/src (unzipped).

## Running the project
```
$ make
# Live build is now running
# Visit http://localhost:7000/play
```


## Credits

### Sprites
* spriters-resource.com
* DMZapp

### Audio
* zapsplat.com