# Source file directory.
SRC=src
# Build output directory.
DIST=dist
# Slurp location.
SLURP=./src/js/JSUtils/Slurp

default: clean
	@# Runs the app for development.
	@mkdir -p $(DIST)
	@make -s build_dev
	cd $(DIST) && python3 -m http.server 7000

install:
	pip install --upgrade $(SLURP)

clean:
	@rm -rf $(DIST)

build_dev:
	@# Fast build for development.
	$(SLURP)/scripts/default_dev_build.sh $(SRC) $(DIST) &
